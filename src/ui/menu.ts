/**
 * Script that handles all top bar menu creation
 */

import { app, MenuItem, Menu, BrowserWindow, shell, dialog } from 'electron'
import { openProject, createProject } from './../project/projectManager'


/**
 * Builds the top bar menu
 * 
 * @param wndw the Browserwindow for the panel
 * @returns the built menu
 */
export const buildMenu = (wndw: BrowserWindow) => {
  const menu = new Menu()
  menu.append(new MenuItem({
    "label": "File",
    "submenu": buildFileMenu(wndw)
  }))
  menu.append(new MenuItem({
    "label": "View",
    "submenu": buildViewMenu(wndw)
  }))
  menu.append(new MenuItem({
    "label": "Help",
    "submenu": buildHelpMenu(wndw)
  }))
  return menu
}


/**
 * Builds the file part of the top bar menu
 * 
 * @param wndw the Browserwindow for the panel
 * @returns the built file part of the top bar menu
 */
const buildFileMenu = (wndw: BrowserWindow) => {
  const menu = new Menu()
  menu.append(new MenuItem({
    "label": "New Project",
    "click": () => {
      createProject(wndw)
    }
  }))
  menu.append(new MenuItem({
    "label": "Open Project",
    "click": () => {
      openProject(wndw)
    }
  }))
  menu.append(new MenuItem({
    "label": "Quit",
    "click": () => {
      app.quit()
    }
  }))
  return menu
}

/**
 * Builds the view part of the top bar menu
 * 
 * @param wndw the Browserwindow for the panel
 * @returns the built view part of the top bar menu
 */
const buildViewMenu = (wndw: BrowserWindow) => {
  const menu = new Menu()
  menu.append(new MenuItem({
    "label": "Toggle Fullscreen",
    "click": () => {
      wndw.setFullScreen(!wndw.isFullScreen())
      wndw.setMenuBarVisibility(true)
    }
  }))
  menu.append(new MenuItem({
    "label": "Minimize",
    "click": () => {
      wndw.minimize()
    }
  }))
  menu.append(new MenuItem({
    "label": "Toggle Maximize",
    "click": () => {
      if (wndw.isMaximized()) {
        wndw.restore()
      } else {
        wndw.maximize()
      }
    }
  }))
  return menu
}

/**
 * Builds the help part of the top bar menu
 * 
 * @param wndw the Browserwindow for the panel
 * @returns the built help part of the top bar menu
 */
const buildHelpMenu = (wndw: BrowserWindow) => {
  const menu = new Menu()
  menu.append(new MenuItem({
    "label": "About",
    "click": () => {
      dialog.showMessageBox(wndw,
        {
          buttons: ["Thanks!", "Open on GitHub", "Message me on Telegram"],
          type: "info",
          message: "This Editor and engine was developed by Nikolai Jay Summers in his free time\n" +
            "If you find any bugs or request a feature or want to help in any other way " +
            "visit the GitLab or message me on Telegram!",
          defaultId: 0
        },
        btn => {
          switch (btn) {
            case 1: shell.openExternal("https://gitlab.com/Vatril/VatsVNLibEditor")
              break
            case 2: shell.openExternal("https://t.me/Vatril")
              break
          }
        }
      )
    }
  }))
  menu.append(new MenuItem({
    "label": "Open Dev Tools",
    "click": () => {
      wndw.webContents.openDevTools()
    }
  }))
  return menu
}
/**
 *
 * Main script
 * starts the actual application
 *
 */

import { app, BrowserWindow, Menu } from 'electron'
import url from 'url'
import path from 'path'
import { buildMenu } from './ui/menu'

/**
 * The main window
 */
let win: BrowserWindow | null

app.setName("Vats VN Engine - Editor")

/**
 * creates the main window
 */
const createWindow = () => {
  win = new BrowserWindow({ darkTheme: true, width: 1280, height: 720, center: true })

  win.loadURL(
    url.format({
      pathname: path.join(app.getAppPath(), 'src', 'pages/startscreen.html'),
      protocol: 'file:',
      slashes: true,
    }),
  );

  Menu.setApplicationMenu(buildMenu(win))


  win.on('closed', () => {
    app.quit()
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  app.quit()
})

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})
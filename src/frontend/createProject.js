
$(() => {
  $("#createButton").on("click", e => {
    const { initProject } = require('electron').remote.require("./src/project/projectManager.ts");
    const { dialog, app } = require('electron').remote;
    const path = require('electron').remote.require("path")
    if (!$("#name").val()) {
      alert("A Project needs name!")
      return;
    }
    dialog.showSaveDialog(require('electron').remote.getCurrentWindow(),
      {
        buttonLabel: "Create Project here",
        title: "Save Project", defaultPath: path.join(app.getPath('documents'), $("#name").val())
      }, file => {
        const data = $("#settings").serializeArray();
        const result = initProject(file,
          {
            name: $("#name").val(),
            aspect: data[0].value,
            changeTransition: data[1].value,
            moveTransition: data[2].value
          });
        if (result) {
          require('electron').remote.getCurrentWindow().close();
        } else {
          dialog.showMessageBox(require('electron').remote.getCurrentWindow(), {
            type: "error",
            message: "Can't create project at " + file,
            title: "Error Creating Project",
            buttons: ["Try Again"]
          })
        }
      });
  });
})
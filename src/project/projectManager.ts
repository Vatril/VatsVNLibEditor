/**
 * This script handles the creation,
 * opening and initializing of projects
 */

import { app, BrowserWindow, dialog } from 'electron'
import url from 'url'
import path from 'path'
import fs from 'fs'

/**
 * Opens a dialog to create a new Project
 * @param parent The parent of the dialog
 */
export const createProject = (parent: BrowserWindow) => {
    const win = new BrowserWindow({ parent: parent, modal: true, darkTheme: true, width: 600, height: 500 })

    win.loadURL(
        url.format({
            pathname: path.join(app.getAppPath(), 'src', 'pages/createProject.html'),
            protocol: 'file:',
            slashes: true,
        }),
    );

    win.setMenuBarVisibility(false)

    win.setAlwaysOnTop(true)
}

/**
 * Opens a dialog to open a project
 * @param parent The parent of the dialog
 */
export const openProject = (parent: BrowserWindow) => {
    dialog.showOpenDialog(parent, {
        buttonLabel: "Open this as Project", properties: ['openDirectory'],
        title: "Open Project", defaultPath: app.getPath('documents')
    }, file => {
        if (file) {
            if (file[0]) {
                try {
                    if (fs.existsSync(path.join(file[0], "ProjectConfig.json"))) {
                        loadProject(file[0], JSON.parse(fs.readFileSync(path.join(file[0], "ProjectConfig.json")).toString()) as ProjectSettings)
                        return
                    }
                } catch (e) {
                    console.error(e)
                }

            }

            dialog.showMessageBox(parent, { buttons: ["OK"], type: "error", message: "Can't open " + file + " as a project.\nAre you sure it contains a ProjectConfig.json?" })
        }
    })
}

/**
 * Loads a project into the editor
 * 
 * @param location the location of the project
 * @param projectSettings the projectsettings
 */
export const loadProject = (location: string, projectSettings: ProjectSettings) => {
    console.log(location, projectSettings)
    //TODO Implement this actually
}

/**
 * The projectsettings hold the importatnt information about a project
 */
export class ProjectSettings {
    name: string
    aspect: string
    changeTransition: string
    moveTransition: string
    createdAt: number
    editedAt: number

    /**
     * Builds the projectsettings
     * 
     * @param name the name of the project
     * @param aspect the aspect ratio of the game
     * @param changeTransition the default changetransition
     * @param moveTransition the default movetransition
     */
    constructor(name: string, aspect: string, changeTransition: string, moveTransition: string) {
        this.name = name
        this.aspect = aspect
        this.changeTransition = changeTransition
        this.moveTransition = moveTransition
        this.createdAt = new Date().getTime()
        this.editedAt = new Date().getTime()
    }
}

module.exports = {
    /**
    * Opens a dialog to create a new Project
    * @param parent The parent of the dialog
    */
    createProject: (parent: BrowserWindow) => {
        createProject(parent)
    },
    /**
    * Opens a dialog to open a project
    * @param parent The parent of the dialog
    */
    openProject: (parent: BrowserWindow) => {
        openProject(parent)
    },
    /**
    * Initializes a folder as a project
    * 
    * @param file the folder that is used
    * @param settings the settings that are used to init
    */
    initProject: (file: string, settings: ProjectSettings) => {
        settings.createdAt = new Date().getTime()
        settings.editedAt = new Date().getTime()
        try {
            fs.mkdirSync(file)
            fs.writeFileSync(path.join(file, "ProjectConfig.json"), JSON.stringify(settings));
            fs.mkdirSync(path.join(file, "res"))
            fs.mkdirSync(path.join(file, "scenes"))
            fs.mkdirSync(path.join(file, "persistantObjects"))
            fs.mkdirSync(path.join(file, "persistantObjects", "characters"))
            fs.mkdirSync(path.join(file, "persistantObjects", "other"))
            loadProject(file, settings)
            return "success"
        } catch (e) {
            console.log(e)
            return false;
        }
    }
}